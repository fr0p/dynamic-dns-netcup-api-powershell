
# Config.json
# {
#     "CUSTOMERNR": "",                             // Enter your netcup customer number here
#     "APIKEY": "",                                 // Enter your API-Key and -Password here - you can generate them in your CCP at https://ccp.netcup.net
#     "APIPASSWORD": "",
#     "DOMAIN": "domain.de",                        // Enter Domain which should be used for dynamic DNS
#     "HOST": "@",                                  // Enter subdomain to be used for dynamic DNS, alternatively '@' for domain root or '*' for wildcard. If the record doesn't exist, the script will create it.
#     "USE_IPV6": false,                            // If set to true, the script will check for your public IPv6 address too and add it as an AAAA-Record / change an existing AAAA-Record for the host. Activate this only if you have IPv6 connectivity, or you *WILL* get errors.
#     "CHANGE_TTL": true,                           // If set to true, this will change TTL to 300 seconds on every run if necessary.
#     "APIURL": "https://ccp.netcup.net/run/webservice/servers/endpoint.php?JSON" // Use netcup DNS REST-API
# }


# Get info about dns zone from netcup domain API, returns result
function InfoDnsZone {
    #($domainname, $customernr, $apikey, $apisessionid)
    
    $infoDnsZoneData = @{
        action = 'infoDnsZone';
        param  = @{
            domainname     = $Config.DOMAIN;
            customernumber = $Config.CUSTOMERNR;
            apikey         = $Config.APIKEY;
            apisessionid   = $Config.SESSION
        }
    }

    $request = ConvertTo-Json -InputObject $infoDnsZoneData
    
    $result = SendRequest -request $request

    if ($result.status -eq "SUCCESS") {
        return $result;
    }

    Write-Error "Error while getting DNS Zone info: $($result.longmessage) Exiting."
    return $false;
}

# Get info about dns records from netcup domain API, returns result
function InfoDnsRecords {
    #($domainname, $customernr, $apikey, $apisessionid)
    # param (
    #     $domainName
    # )

    $infoDnsRecordsData = @{
        action = 'infoDnsRecords';
        param  = @{
            domainname     = $Config.DOMAIN;
            customernumber = $Config.CUSTOMERNR;
            apikey         = $Config.APIKEY;
            apisessionid   = $Config.SESSION
        }
    }

    $request = ConvertTo-Json -InputObject $infoDnsRecordsData
    
    $result = SendRequest -request $request

    if ($result.status -eq "SUCCESS") {
        return $result;
    }

    Write-Error "Error while getting DNS Record info: $($result.longmessage) Exiting."
    return $false;
}

# Updates DNS Zone using the netcup domain API and returns boolean
function UpdateDnsZone {
    #($domainname, $customernr, $apikey, $apisessionid, $dnszone)
    param (
        $dnsZone
    )

    $updateDnsZoneData = @{
        action = 'updateDnsZone';
        param  = @{
            domainname     = $Config.DOMAIN;
            customernumber = $Config.CUSTOMERNR;
            apikey         = $Config.APIKEY;
            apisessionid   = $Config.SESSION;
            dnszone        = $dnsZone
        }
    }

    $request = ConvertTo-Json -InputObject $updateDnsZoneData -Depth 10
    
    $result = SendRequest -request $request

    if ($result.status -eq "SUCCESS") {
        return $result;
    }

    Write-Error "Error while updating DNS Zone: $($result.longmessage) Exiting."
    return $false;
}

# Updates DNS records using the netcup domain API and returns boolean
function UpdateDnsRecords {
    #($domainname, $customernr, $apikey, $apisessionid, $dnsrecords)

    param (
        $dnsrecords
    )

    $updateDnsZoneData = @{
        action = 'updateDnsRecords';
        param  = @{
            domainname     = $Config.DOMAIN;
            customernumber = $Config.CUSTOMERNR;
            apikey         = $Config.APIKEY;
            apisessionid   = $Config.SESSION;
            dnsrecordset   = @{
                dnsrecords = $dnsrecords
            }
        }
    }

    $request = ConvertTo-Json -InputObject $updateDnsZoneData -Depth 10
    
    $result = SendRequest -request $request

    if ($result.status -eq "SUCCESS") {
        return $result;
    }

    Write-Error "Error while updating DNS Records: $($result.longmessage) Exiting."
    return $false;
}




function Login {

    # param (
    #     $customerNr,
    #     $apiKey,
    #     $apiPassword
    # )

    $logindata = @{
        action = 'login';
        param  = @{
            customernumber = $Config.CUSTOMERNR;
            apikey         = $Config.APIKEY;
            apipassword    = $Config.APIPASSWORD
        }
    }

    $request = ConvertTo-Json -InputObject $logindata
    
    $result = SendRequest -request $request

    if ($result.status -eq "SUCCESS") {
        return $result.responsedata.apisessionid;
    }

    # Error from API: "More than 180 requests per minute. Please wait and retry later. Please contact our customer service to find out if the limitation of requests can be increased."
    if ($result.statuscode -eq 4013) {
        $result.longmessage = $result.longmessage + ' [ADDITIONAL INFORMATION: This error from the netcup DNS API also often indicates that you have supplied wrong API credentials. Please check them in the config file.]'
    }

    Write-Host "Error while logging in: $($result.longmessage) Exiting."
    return $false;
}

# Logout of netcup domain API, returns boolean
function Logout {

    param (
        $apiSession
    )

    $logoutdata = @{
        action = 'logout';
        param  = @{
            customernumber = $Config.CUSTOMERNR;
            apikey         = $Config.APIKEY;
            apisessionid   = $apiSession
        }
    }

    $request = ConvertTo-Json -InputObject $logoutdata

    $logoutdata

    $result = SendRequest -request $request

    if ($result.status -eq "SUCCESS") {
        return $true;
    }

    Write-Host "Error while logging out: $($result.longmessage) Exiting."
    return $false;
}

function ValidateIpAddress {

    param (
        $ip
    )

    # Valid IPAddress
    return [ipaddress]::TryParse($ip, [ref][ipaddress]::Loopback)

}

# Requires -Version 2.0
function Test-MrIpAddress {

    <#
    .SYNOPSIS
        Tests one or more IP Addresses to determine if they are valid.
    
    .DESCRIPTION
        Test-MrIpAddress is an advanced function that tests one or more IP Addresses to determine if
        they are valid. The detailed parameter can be used to return additional information about the IP.
    
    .PARAMETER IpAddress
        One or more IP Addresses to test. This parameter is mandatory.
    
    .PARAMETER Detailed
        Switch parameter to return detailed infomation about the IP Address instead of a boolean.
    
    .EXAMPLE
         Test-MrIpAddress -IpAddress '192.168.0.1', '192.168.0.256'
    
    .EXAMPLE
         Test-MrIpAddress -IpAddress '192.168.0.1' -Detailed
    
    .EXAMPLE
         '::1', '192.168.0.256' | Test-MrIpAddress
    
    .INPUTS
        String
    
    .OUTPUTS
        Boolean
    
    .NOTES
        Author:  Mike F Robbins
        Website: http://mikefrobbins.com
        Twitter: @mikefrobbins
    #>
    
    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $true,
            ValueFromPipeLine = $true)]
        [string[]]$IpAddress,
    
        [switch]$Detailed
    )
    
    PROCESS {
    
        foreach ($Ip in $IpAddress) {
    
            try {
                $Results = $Ip -match ($DetailedInfo = [IPAddress]$Ip)
            }
            catch {
                Write-Output $false
                Continue
            }
    
            if (-not($PSBoundParameters.Detailed)) {
                Write-Output $Results
            }
            else {
                Write-Output $DetailedInfo
            }
    
        }
    
    }
    
}

# Returns current public IPv4 address.
function GetCurrentPublicIPv4 {

    $ip = $(Invoke-WebRequest -Uri "https://api.ipify.org").Content.Trim()

    if ( (ValidateIpAddress -ip $ip) -eq $true ) {
        return $ip;
    }
    
    Write-Warning "https://api.ipify.org didn't return a valid IPv4 address. Trying fallback API https://ip4.seeip.org"

    # If IP is invalid, try another API
    # The API adds an empty line, so we remove that with rtrim
    $ip = $(Invoke-WebRequest -Uri "https://ip4.seeip.org").Content.Trim()

    if ( (ValidateIpAddress -ip $ip) -eq $true ) {
        return $ip;
    }
    
    # Still no valid IP?
    return $false;
}

# Returns current public IPv6 address
function GetCurrentPublicIPv6 {
    
    $ip = $(Invoke-WebRequest -Uri "https://ip6.seeip.org").Content.Trim()

    if ( (Test-MrIpAddress $ip) -eq $true ) {
        return $ip;
    }

    Write-Warning "https://ip6.seeip.org didn't return a valid IPv6 address."
    # If IP is invalid, try another API
    $ip = $(Invoke-WebRequest -Uri "https://v6.ident.me/").Content.Trim()

    # Let's check the result of the second API
    if ( (Test-MrIpAddress $ip) -eq $true ) {
        return $ip;
    }

    # Still no valid IP?
    return $false;
}

function SendRequest {
    param (
        #$uri,
        $request
    )
    
    # $headers = @{
    #     'Content-Type' = 'application/json'
    #     #'token' = 'TokenValue'
    # }

    $response = Invoke-RestMethod -Method POST -ContentType "application/json" -Uri $Config.APIURL -Body $request -TimeoutSec 30 #-Headers $headers 

    return $response;

}

$currentDir = (Get-Location).Path
Write-Host "Working Path: $currentDir"

$Config = Get-Content -Path $currentDir\config.json | ConvertFrom-Json

# Output Config for debugging
#$Config

Write-Host "============================================="
Write-Host "Running dynamic DNS client for netcup 2.0"
Write-Host "This script is not affiliated with netcup."
Write-Host "============================================="
Write-Host ""

# Get current IPv4 address
$ipv4 = GetCurrentPublicIPv4
if ($ipv4 -eq $false) {
    Write-Error "Main API and fallback API didn't return a valid IPv4 address. Exiting."
    exit 1;
}

if ($Config.USE_IPV6 -eq $true) {

    Write-Error "IPv6 is currently not implemented"
    return 1

    # Get current IPv6 address
    $ipv6 = GetCurrentPublicIPv6
    if ($ipv6 -eq $false) {
        Write-Error "Main API and fallback API didn't return a valid IPv6 address. Do you have IPv6 connectivity? If not, please disable USE_IPV6 in config.php. Exiting."
        exit 1;
    }
}

Write-Host "Updating DNS records for host '$($Config.HOST)' on domain '$($Config.DOMAIN)'"

# Login
$sessionId = Login
if (!$sessionId) {
    Write-Host "Error logging in."
    return 1;
}

Write-Host "Logged in with Session-Id: $sessionId"
#$Config.Add('SESSION', $sessionId)

$Config | Add-Member -MemberType NoteProperty -Name 'SESSION' -Value $sessionId




# Let's get infos about the DNS zone
$infoDnsZone = InfoDnsZone
if ($infoDnsZone -eq $false) {
    return 1
}
else {
    Write-Host "Successfully received Domain info."
}

# TTL Warning
if ($Config.CHANGE_TTL -eq $false -and $infoDnsZone.responsedata.ttl -gt 300) {
    Write-Warning "TTL is higher than 300 seconds - this is not optimal for dynamic DNS, since DNS updates will take a long time. Ideally, change TTL to lower value. You may set CHANGE_TTL to True in config.php, in which case TTL will be set to 300 seconds automatically."
}

# If user wants it, then we lower TTL, in case it doesn't have correct value
if ($Config.CHANGE_TTL -eq $true -and $infoDnsZone.responsedata.ttl -ne 300) {
    $infoDnsZone.responsedata.ttl = 300;

    $updateResult = UpdateDnsZone -dnsZone $infoDnsZone.responsedata
    if ($updateResult -ne $false) {
        Write-Host "Lowered TTL to 300 seconds successfully."
    }
    else {
        Write-Error "Failed to set TTL... Continuing."
    }
}

# Let's get the DNS record data.
$infoDnsRecords = InfoDnsRecords
if ($infoDnsRecords -ne $false) {
    Write-Host "Successfully received DNS record data."
}
else {
    return 1
}

# Find the host defined in config.php
$foundHostsV4 = @();

foreach ($record in $infoDnsRecords.responsedata.dnsrecords) {
    if ($record.hostname -eq $Config.HOST -and $record.type -eq "A") {
        $foundHostsV4 += @{
            id           = $record.id;
            hostname     = $record.hostname;
            type         = $record.type;
            priority     = $record.priority;
            destination  = $record.destination;
            deleterecord = $record.deleterecord;
            state        = $record.state
        }
    }
}

# //If we can't find the host, create it.
if ($foundHostsV4.Count -eq 0) {
    Write-Host "A record for host $($Config.HOST) doesn't exist, creating necessary DNS record."
    $foundHostsV4 += @{
        hostname    = $Config.HOST;
        type        = 'A';
        destination = 'newly created Record'
    }
}

# If the host with A record exists more than one time...
if ($foundHostsV4.Count -gt 1) {
    Write-Host "Found multiple A records for the host $($Config.HOST) - Please specify a host for which only a single A record exists in config.php. Exiting."
    return 1
}

$ipv4change = $false;

# Has the IP changed?
foreach ($record in $foundHostsV4) {
    if ($record.destination -ne $ipv4) {
        # Yes, it has changed.
        $ipv4change = $true;
        Write-Host "IPv4 address has changed. Before: $($record.destination); Now: $($ipv4)"
    }
    else {
        # No, it hasn't changed.
        Write-Host "IPv4 address hasn't changed. Current IPv4 address: $($ipv4)"
    }
}

# Yes, it has changed.
if ($ipv4change -eq $true) {
    $foundHostsV4[0]['destination'] = $ipv4;
    # Update the record

    $updateResult = UpdateDnsRecords -dnsrecords $foundHostsV4

    if ($updateResult -ne $false) {
        Write-Host "IPv4 address updated successfully!"
    }
    else {
        return 1
    }
}





$logout_success = logout -apiSession $sessionId

if ($logout_success) {
    Write-Host "Logout successful."
}
else {
    Write-Host "Error while logout."
}

# if ($apisessionid = login(CUSTOMERNR, APIKEY, APIPASSWORD)) {
#     outputStdout("Logged in successfully!");
# }
# else {
#     exit(1);
# }